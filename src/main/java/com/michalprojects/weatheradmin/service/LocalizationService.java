package com.michalprojects.weatheradmin.service;

import com.michalprojects.weatheradmin.downloader.LocalizationViaKeyDownloader;
import com.michalprojects.weatheradmin.downloader.SearchCityKey;
import com.michalprojects.weatheradmin.model.LocalizationViaKeyModels.LocalizationViaKeyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocalizationService {

    private static LocalizationViaKeyDownloader localizationViaKeyDownloader;
    private static SearchCityKey searchCityKey;

    @Autowired
    public LocalizationService(LocalizationViaKeyDownloader localizationViaKeyDownloader, SearchCityKey searchCityKey) {
        this.localizationViaKeyDownloader = localizationViaKeyDownloader;
        this.searchCityKey = searchCityKey;
    }

    public static LocalizationViaKeyDto downloadLocalizationInfo(String localizationKey){
        return localizationViaKeyDownloader.download(localizationKey);
    }

    public static LocalizationViaKeyDto searchLocalization (String cityName){
        if (cityName == null && cityName.isEmpty()){
            return null;
        }
        return searchCityKey.findCity(cityName);
    }
}
