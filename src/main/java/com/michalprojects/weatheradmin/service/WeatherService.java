package com.michalprojects.weatheradmin.service;

import com.michalprojects.weatheradmin.downloader.CurrentConditionsDownloader;
import com.michalprojects.weatheradmin.model.CurrentConditionsModels.CurrentConditionsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeatherService {

    private static CurrentConditionsDownloader currentConditionsDownloader;

    @Autowired
    public WeatherService(CurrentConditionsDownloader currentConditionsDownloader) {
        this.currentConditionsDownloader = currentConditionsDownloader;
    }

    public static CurrentConditionsDto downloadCurrentConditions(String locationKey) {
        CurrentConditionsDto currentConditionsDto = currentConditionsDownloader.download(locationKey);
        return currentConditionsDto;
    }
}

