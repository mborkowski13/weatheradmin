package com.michalprojects.weatheradmin.model.HtmlModels;

public class AdditionalLocation {
    private String locationName;

    public AdditionalLocation() {
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
