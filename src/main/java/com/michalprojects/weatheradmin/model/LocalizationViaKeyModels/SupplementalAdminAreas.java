package com.michalprojects.weatheradmin.model.LocalizationViaKeyModels;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SupplementalAdminAreas {
    @JsonProperty("Level") private Integer level;
    @JsonProperty("LocalizedName") private String localizedName;
    @JsonProperty("EnglishName") private String englishName;

    public SupplementalAdminAreas() {
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }
}
