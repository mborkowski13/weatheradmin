package com.michalprojects.weatheradmin.model.LocalizationViaKeyModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoPosition {
    @JsonProperty("Latitude") private BigDecimal latitude;
    @JsonProperty("Longitude") private BigDecimal longitude;
    @JsonProperty("Elevation") private Elevation elevation;

    public GeoPosition() {
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public Elevation getElevation() {
        return elevation;
    }

    public void setElevation(Elevation elevation) {
        this.elevation = elevation;
    }
}
