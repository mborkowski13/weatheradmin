package com.michalprojects.weatheradmin.model.LocalizationViaKeyModels;

public class LocalizationViaKeyDto {
    private String locationKey;
    private String locationName;
    private String countryId;

    public LocalizationViaKeyDto() {
    }

    public String getLocationKey() {
        return locationKey;
    }

    public void setLocationKey(String locationKey) {
        this.locationKey = locationKey;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    @Override
    public String toString() {
        return "LocalizationViaKeyDto{" +
                "locationKey='" + locationKey + '\'' +
                ", locationName='" + locationName + '\'' +
                ", countryId='" + countryId + '\'' +
                '}';
    }
}
