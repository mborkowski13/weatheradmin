package com.michalprojects.weatheradmin.model.LocalizationViaKeyModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.michalprojects.weatheradmin.model.CurrentConditionsModels.Imperial;
import com.michalprojects.weatheradmin.model.CurrentConditionsModels.Metric;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Elevation {
    @JsonProperty("Matric") private Metric metric;
    @JsonProperty("Imperial") private Imperial imperial;

    public Elevation() {
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public Imperial getImperial() {
        return imperial;
    }

    public void setImperial(Imperial imperial) {
        this.imperial = imperial;
    }
}
