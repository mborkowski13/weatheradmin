package com.michalprojects.weatheradmin.model.LocalizationViaKeyModels;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.michalprojects.weatheradmin.model.LocalizationViaKeyModels.*;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocalizationInfoSearchViaKeyJson {
    @JsonProperty("Version") private Integer version;
    @JsonProperty("Key") private String locationKey;
    @JsonProperty("Type") private String type;
    @JsonProperty("Rank") private Integer rank;
    @JsonProperty("LocalizedName") private String localizedName;
    @JsonProperty("EnglishName") private String englishName;
    @JsonProperty("PrimaryPostalCode") private String primaryPostalCode;
    @JsonProperty("Region") private Region region;
    @JsonProperty("Country") private Region country;
    @JsonProperty("AdministrativeArea") private AdministrativeArea administrativeArea;
    @JsonProperty("TimeZone") private TimeZone timeZone;
    @JsonProperty("GeoPosition") private GeoPosition geoPosition;
    @JsonProperty("IsAlias") private boolean isAlias;
    @JsonProperty("ParentCity") private ParentCity parentCity;
    @JsonProperty("SupplementalAdminAreas") private List<SupplementalAdminAreas> supplementalAdminAreas;
    @JsonProperty("DataSets") private List<String> dataSets;

    public LocalizationInfoSearchViaKeyJson() {
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getLocationKey() {
        return locationKey;
    }

    public void setLocationKey(String locationKey) {
        this.locationKey = locationKey;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getPrimaryPostalCode() {
        return primaryPostalCode;
    }

    public void setPrimaryPostalCode(String primaryPostalCode) {
        this.primaryPostalCode = primaryPostalCode;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Region getCountry() {
        return country;
    }

    public void setCountry(Region country) {
        this.country = country;
    }

    public AdministrativeArea getAdministrativeArea() {
        return administrativeArea;
    }

    public void setAdministrativeArea(AdministrativeArea administrativeArea) {
        this.administrativeArea = administrativeArea;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public GeoPosition getGeoPosition() {
        return geoPosition;
    }

    public void setGeoPosition(GeoPosition geoPosition) {
        this.geoPosition = geoPosition;
    }

    public boolean isAlias() {
        return isAlias;
    }

    public void setAlias(boolean alias) {
        isAlias = alias;
    }

    public ParentCity getParentCity() {
        return parentCity;
    }

    public void setParentCity(ParentCity parentCity) {
        this.parentCity = parentCity;
    }

    public List<SupplementalAdminAreas> getSupplementalAdminAreas() {
        return supplementalAdminAreas;
    }

    public void setSupplementalAdminAreas(List<SupplementalAdminAreas> supplementalAdminAreas) {
        this.supplementalAdminAreas = supplementalAdminAreas;
    }

    public List<String> getDataSets() {
        return dataSets;
    }

    public void setDataSets(List<String> dataSets) {
        this.dataSets = dataSets;
    }
}
