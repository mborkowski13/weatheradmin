package com.michalprojects.weatheradmin.model.LocalizationViaKeyModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeZone {
    @JsonProperty("Code") private String code;
    @JsonProperty("Name") private String name;
    @JsonProperty("GmtOffset") private Integer gmtOffset;
    @JsonProperty("IsDaylightSaving") private boolean isDaylightSaving;
    @JsonProperty("NextOffsetChange") private String nextOffsetChange;

    public TimeZone() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGmtOffset() {
        return gmtOffset;
    }

    public void setGmtOffset(Integer gmtOffset) {
        this.gmtOffset = gmtOffset;
    }

    public boolean isDaylightSaving() {
        return isDaylightSaving;
    }

    public void setDaylightSaving(boolean daylightSaving) {
        isDaylightSaving = daylightSaving;
    }

    public String getNextOffsetChange() {
        return nextOffsetChange;
    }

    public void setNextOffsetChange(String nextOffsetChange) {
        this.nextOffsetChange = nextOffsetChange;
    }
}
