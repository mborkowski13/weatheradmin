package com.michalprojects.weatheradmin.model.CurrentConditionsModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Wind {
    @JsonProperty("Direction") private Direction direction;
    @JsonProperty("Speed") private Temperature speed;

    public Wind() {
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Temperature getSpeed() {
        return speed;
    }

    public void setSpeed(Temperature speed) {
        this.speed = speed;
    }
}
