package com.michalprojects.weatheradmin.model.CurrentConditionsModels;

import java.math.BigDecimal;

public class CurrentConditionsDto {

    private String localObservationDateTime;
    private String weatherDescription;
    private Integer weatherIcon;
    private String weatherIconFinal;
    private boolean isItDayTime;
    private BigDecimal temperature;
    private String unit;
    private String locationName;

    public CurrentConditionsDto() {
    }

    public String getLocalObservationDateTime() {
        return localObservationDateTime;
    }

    public void setLocalObservationDateTime(String localObservationDateTime) {
        this.localObservationDateTime = localObservationDateTime;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }


    public String getWeatherIconFinal() {
        return weatherIconFinal;
    }

    public void setWeatherIconFinal(String weatherIconFinal) {
        this.weatherIconFinal = weatherIconFinal;
    }

    public Integer getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(Integer weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    public boolean isItDayTime() {
        return isItDayTime;
    }

    public void setItDayTime(boolean itDayTime) {
        isItDayTime = itDayTime;
    }

    public BigDecimal getTemperature() {
        return temperature;
    }

    public void setTemperature(BigDecimal temperature) {
        this.temperature = temperature;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    @Override
    public String toString() {
        return "CurrentConditionsDto{" +
                "localObservationDateTime='" + localObservationDateTime + '\'' +
                ", weatherDescription='" + weatherDescription + '\'' +
                ", weatherIcon=" + weatherIcon +
                ", weatherIconFinal='" + weatherIconFinal + '\'' +
                ", isItDayTime=" + isItDayTime +
                ", temperature=" + temperature +
                ", unit='" + unit + '\'' +
                ", locationName='" + locationName + '\'' +
                '}';
    }
}
