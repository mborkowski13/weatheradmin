package com.michalprojects.weatheradmin.model.CurrentConditionsModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TemperatureSummary {
    @JsonProperty("Past6HourRange") private Range past6HourRange;
    @JsonProperty("Past12HourRange") private Range past12HourRange;
    @JsonProperty("Past24HourRange") private Range past24HourRange;

    public TemperatureSummary() {
    }

    public Range getPast6HourRange() {
        return past6HourRange;
    }

    public void setPast6HourRange(Range past6HourRange) {
        this.past6HourRange = past6HourRange;
    }

    public Range getPast12HourRange() {
        return past12HourRange;
    }

    public void setPast12HourRange(Range past12HourRange) {
        this.past12HourRange = past12HourRange;
    }

    public Range getPast24HourRange() {
        return past24HourRange;
    }

    public void setPast24HourRange(Range past24HourRange) {
        this.past24HourRange = past24HourRange;
    }
}
