package com.michalprojects.weatheradmin.model.CurrentConditionsModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WindGust {
    @JsonProperty("Speed") private Temperature speed;

    public WindGust() {
    }

    public Temperature getSpeed() {
        return speed;
    }

    public void setSpeed(Temperature speed) {
        this.speed = speed;
    }
}
