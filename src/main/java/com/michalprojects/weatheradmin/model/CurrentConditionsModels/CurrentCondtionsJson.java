package com.michalprojects.weatheradmin.model.CurrentConditionsModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentCondtionsJson {

    @JsonProperty("LocalObservationDateTime") private String localObservationDateTime;
    @JsonProperty("EpochTime") private Integer epochTime;
    @JsonProperty("WeatherText") private String weatherText;
    @JsonProperty("WeatherIcon") private Integer weatherIcon;
    @JsonProperty("IsDayTime") private boolean isDayTime;
    @JsonProperty("Temperature")private Temperature temperature;
    @JsonProperty("RealFeelTemperature")private Temperature realFeelTemperature;
    @JsonProperty("RealFeelTemperatureShade")private Temperature getRealFeelTemperatureShade;
    @JsonProperty("RelativeHumidity") private Integer relativeHumidity;
    @JsonProperty("DewPoint")private Temperature dewPoint;
    @JsonProperty("Wind")private Wind wind;
    @JsonProperty("WindGust")private WindGust windGust;
    @JsonProperty("UVIndex")private Integer uvIndex;
    @JsonProperty("Visibility")private Temperature visibility;
    @JsonProperty("ObstructionsToVisibility")private String obstructionsToVisibility;
    @JsonProperty("CloudCover")private Integer cloudCover;
    @JsonProperty("Ceiling")private Temperature ceiling;
    @JsonProperty("Pressure")private Temperature pressure;
    @JsonProperty("PressureTendency")private PressureTendency PressureTendency;
    @JsonProperty("Past24HourTemperatureDeparture")private Temperature past24HourTemperatureDeparture;
    @JsonProperty("ApparentTemperature")private Temperature apparentTemperature;
    @JsonProperty("WindChillTemperature")private Temperature windChillTemperature;
    @JsonProperty("WetBulbTemperature")private Temperature wetBulbTemperature;
    @JsonProperty("Precip1hr")private Temperature precip1hr;
    @JsonProperty("PrecipitationSummary")private PrecipitationSummary precipitationSummary;
    @JsonProperty("TemperatureSummary") private TemperatureSummary temperatureSummary;
    @JsonProperty("MobileLink") private String mobileLink;
    @JsonProperty("Link") private String link;

    public CurrentCondtionsJson() {
    }

    public String getLocalObservationDateTime() {
        return localObservationDateTime;
    }

    public void setLocalObservationDateTime(String localObservationDateTime) {
        this.localObservationDateTime = localObservationDateTime;
    }

    public Integer getEpochTime() {
        return epochTime;
    }

    public void setEpochTime(Integer epochTime) {
        this.epochTime = epochTime;
    }

    public String getWeatherText() {
        return weatherText;
    }

    public void setWeatherText(String weatherText) {
        this.weatherText = weatherText;
    }

    public Integer getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(Integer weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    public boolean isDayTime() {
        return isDayTime;
    }

    public void setDayTime(boolean dayTime) {
        isDayTime = dayTime;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public String getMobileLink() {
        return mobileLink;
    }

    public void setMobileLink(String mobileLink) {
        this.mobileLink = mobileLink;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
