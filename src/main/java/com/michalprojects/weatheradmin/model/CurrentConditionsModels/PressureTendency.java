package com.michalprojects.weatheradmin.model.CurrentConditionsModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PressureTendency {
    @JsonProperty("LocalizedText") private String localizedText;
    @JsonProperty("Code") private String code;

    public PressureTendency() {
    }

    public String getLocalizedText() {
        return localizedText;
    }

    public void setLocalizedText(String localizedText) {
        this.localizedText = localizedText;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
