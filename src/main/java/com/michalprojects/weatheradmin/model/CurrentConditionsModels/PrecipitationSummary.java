package com.michalprojects.weatheradmin.model.CurrentConditionsModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PrecipitationSummary {
    @JsonProperty("Precipitation") private Temperature Precipitation;
    @JsonProperty("PastHour") private Temperature pastHour;
    @JsonProperty("Past3Hours") private Temperature past3Hours;
    @JsonProperty("Past6Hours") private Temperature past6Hours;
    @JsonProperty("Past9Hours") private Temperature past9Hours;
    @JsonProperty("Past12Hours") private Temperature past12Hours;
    @JsonProperty("Past18Hours") private Temperature past18Hours;
    @JsonProperty("Past24Hours") private Temperature past24Hours;

    public PrecipitationSummary() {
    }

    public Temperature getPrecipitation() {
        return Precipitation;
    }

    public void setPrecipitation(Temperature precipitation) {
        Precipitation = precipitation;
    }

    public Temperature getPastHour() {
        return pastHour;
    }

    public void setPastHour(Temperature pastHour) {
        this.pastHour = pastHour;
    }

    public Temperature getPast3Hours() {
        return past3Hours;
    }

    public void setPast3Hours(Temperature past3Hours) {
        this.past3Hours = past3Hours;
    }

    public Temperature getPast6Hours() {
        return past6Hours;
    }

    public void setPast6Hours(Temperature past6Hours) {
        this.past6Hours = past6Hours;
    }

    public Temperature getPast9Hours() {
        return past9Hours;
    }

    public void setPast9Hours(Temperature past9Hours) {
        this.past9Hours = past9Hours;
    }

    public Temperature getPast12Hours() {
        return past12Hours;
    }

    public void setPast12Hours(Temperature past12Hours) {
        this.past12Hours = past12Hours;
    }

    public Temperature getPast18Hours() {
        return past18Hours;
    }

    public void setPast18Hours(Temperature past18Hours) {
        this.past18Hours = past18Hours;
    }

    public Temperature getPast24Hours() {
        return past24Hours;
    }

    public void setPast24Hours(Temperature past24Hours) {
        this.past24Hours = past24Hours;
    }
}
