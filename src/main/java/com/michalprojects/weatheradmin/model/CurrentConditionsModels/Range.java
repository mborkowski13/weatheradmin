package com.michalprojects.weatheradmin.model.CurrentConditionsModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Range {
    @JsonProperty("Minimum") private Temperature minimum;
    @JsonProperty("Maximum") private Temperature maximum;

    public Range() {
    }

    public Temperature getMinimum() {
        return minimum;
    }

    public void setMinimum(Temperature minimum) {
        this.minimum = minimum;
    }

    public Temperature getMaximum() {
        return maximum;
    }

    public void setMaximum(Temperature maximum) {
        this.maximum = maximum;
    }
}
