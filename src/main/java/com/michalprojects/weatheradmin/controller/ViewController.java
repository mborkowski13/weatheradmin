package com.michalprojects.weatheradmin.controller;

import com.michalprojects.weatheradmin.model.CurrentConditionsModels.CurrentConditionsDto;
import com.michalprojects.weatheradmin.model.HtmlModels.AdditionalLocation;
import com.michalprojects.weatheradmin.model.LocalizationViaKeyModels.LocalizationViaKeyDto;
import com.michalprojects.weatheradmin.service.LocalizationService;
import com.michalprojects.weatheradmin.service.WeatherService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ViewController {

    @GetMapping("/")
    public String getMainView(Model model){
        model.addAttribute("additionalLocation", new AdditionalLocation());
        return "Index.html";
    }

    @GetMapping("/conditions")
    public String getWeatherView(@RequestParam(name = "selectedCities", required = false) String selectedCities[], @RequestParam(name = "locationName", required = false)String additionalLocation, Model model){
        List<CurrentConditionsDto>currentConditionsDtos = new ArrayList<>();
        List<LocalizationViaKeyDto>localizationViaKeyDtos = new ArrayList<>();
        if(selectedCities.length == 0 && selectedCities == null){
            return "redirect:/";
        }
        if(additionalLocation.isEmpty() || additionalLocation == null){
            for (int i = 0; i < selectedCities.length; i++) {
                currentConditionsDtos.add(WeatherService.downloadCurrentConditions(selectedCities[i]));
                localizationViaKeyDtos.add(LocalizationService.downloadLocalizationInfo(selectedCities[i]));
            }
            for (int i = 0; i <localizationViaKeyDtos.size() ; i++) {
                currentConditionsDtos.get(i).setLocationName(localizationViaKeyDtos.get(i).getLocationName());
            }

        } else{
            for (int i = 0; i < selectedCities.length; i++) {
                currentConditionsDtos.add(WeatherService.downloadCurrentConditions(selectedCities[i]));
                localizationViaKeyDtos.add(LocalizationService.downloadLocalizationInfo(selectedCities[i]));
            }
            for (int i = 0; i <localizationViaKeyDtos.size() ; i++) {
                currentConditionsDtos.get(i).setLocationName(localizationViaKeyDtos.get(i).getLocationName());
            }
            LocalizationViaKeyDto additionalCity = LocalizationService.searchLocalization(additionalLocation);
            CurrentConditionsDto addCityDto = WeatherService.downloadCurrentConditions(additionalCity.getLocationKey());
            addCityDto.setLocationName(additionalCity.getLocationName());
            currentConditionsDtos.add(addCityDto);

        }
        model.addAttribute("weathers", currentConditionsDtos);
        return "AdvancedAdminConsole.html";
    }
}
