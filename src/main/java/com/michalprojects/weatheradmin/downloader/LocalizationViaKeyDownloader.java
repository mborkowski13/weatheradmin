package com.michalprojects.weatheradmin.downloader;

import com.michalprojects.weatheradmin.model.LocalizationViaKeyModels.LocalizationInfoSearchViaKeyJson;
import com.michalprojects.weatheradmin.model.LocalizationViaKeyModels.LocalizationViaKeyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class LocalizationViaKeyDownloader {

    private RestTemplate restTemplate;

    private String apiKey = "mIuttAeF9vqFBbHvVNBO5HweepuN7jsb";


    private String URL = "http://dataservice.accuweather.com/locations/v1/{locationKey}?apikey={apikey}&language=pl-pl";
    @Autowired
    public LocalizationViaKeyDownloader(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public LocalizationViaKeyDto download (String locationKey){
        LocalizationViaKeyDto localizationViaKeyDto = new LocalizationViaKeyDto();
        try{
            LocalizationInfoSearchViaKeyJson jsonTemplate = restTemplate.getForObject(URL,LocalizationInfoSearchViaKeyJson.class, locationKey, apiKey);
            localizationViaKeyDto.setLocationKey(jsonTemplate.getLocationKey());
            localizationViaKeyDto.setLocationName(jsonTemplate.getLocalizedName());
            localizationViaKeyDto.setCountryId(jsonTemplate.getCountry().getId());
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return localizationViaKeyDto;
    }

}
