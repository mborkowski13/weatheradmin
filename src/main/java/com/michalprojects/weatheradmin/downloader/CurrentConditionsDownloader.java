package com.michalprojects.weatheradmin.downloader;

import com.michalprojects.weatheradmin.model.CurrentConditionsModels.CurrentConditionsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.michalprojects.weatheradmin.model.CurrentConditionsModels.CurrentCondtionsJson;

@Component
public class CurrentConditionsDownloader {

    private RestTemplate restTemplate;

    private String apiKey = "mIuttAeF9vqFBbHvVNBO5HweepuN7jsb";

    private String URL = "http://dataservice.accuweather.com/currentconditions/v1/{locationKey}?apikey={apiKey}&language=pl-pl";

    @Autowired
    public CurrentConditionsDownloader(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public CurrentConditionsDto download(String locationKey){
        CurrentConditionsDto currentConditionsDto = new CurrentConditionsDto();

        try{
            CurrentCondtionsJson[] jsonObjects = restTemplate.getForObject(URL, CurrentCondtionsJson[].class,locationKey, apiKey);
            String[] localTimeSplit = jsonObjects[0].getLocalObservationDateTime().split("T");
            String[] finalSplit = localTimeSplit[1].split("\\+");
            currentConditionsDto.setLocalObservationDateTime(finalSplit[0]);
            currentConditionsDto.setTemperature(jsonObjects[0].getTemperature().getMetric().getValue());
            currentConditionsDto.setUnit(jsonObjects[0].getTemperature().getMetric().getUnit());
            currentConditionsDto.setWeatherDescription(jsonObjects[0].getWeatherText());
            currentConditionsDto.setWeatherIcon(jsonObjects[0].getWeatherIcon());
            currentConditionsDto.setItDayTime(jsonObjects[0].isDayTime());
            currentConditionsDto.setWeatherIconFinal(Integer.toString(jsonObjects[0].getWeatherIcon())+".png");

        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        return currentConditionsDto;
    }
}
