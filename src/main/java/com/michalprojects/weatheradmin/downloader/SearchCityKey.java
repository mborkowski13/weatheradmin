package com.michalprojects.weatheradmin.downloader;

import com.michalprojects.weatheradmin.model.LocalizationViaKeyModels.LocalizationInfoSearchViaKeyJson;
import com.michalprojects.weatheradmin.model.LocalizationViaKeyModels.LocalizationViaKeyDto;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SearchCityKey {

    private RestTemplate restTemplate;

    private String apiKey = "mIuttAeF9vqFBbHvVNBO5HweepuN7jsb";

    private String URL = "http://dataservice.accuweather.com/locations/v1/cities/search?apikey={apikey}&q={cityName}&language=pl-pl";

    public SearchCityKey(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public LocalizationViaKeyDto findCity(String cityName) {
        LocalizationViaKeyDto localizationViaKeyDto = new LocalizationViaKeyDto();
        try {
            LocalizationInfoSearchViaKeyJson[] jsonTemplate = restTemplate.getForObject(URL, LocalizationInfoSearchViaKeyJson[].class, apiKey, cityName);
            localizationViaKeyDto.setLocationKey(jsonTemplate[0].getLocationKey());
            localizationViaKeyDto.setLocationName(jsonTemplate[0].getLocalizedName());
            localizationViaKeyDto.setCountryId(jsonTemplate[0].getCountry().getId());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return localizationViaKeyDto;
    }
}
